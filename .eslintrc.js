// 要安装以下npm包才有效
// eslint
// eslint-config-prettier
// eslint-plugin-prettier
// @typescript-eslint/eslint-plugin
// @typescript-eslint/parser

module.exports = {
  parser: "@typescript-eslint/parser", //定义ESLint的解析器
  extends: [
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended'
  ], //定义文件继承的子规范
  plugins: ["@typescript-eslint"], //定义了该eslint文件所依赖的插件
  env: {
    //指定代码的运行环境
    browser: false,
    node: true
  },
  globals: {
    // ENV: true
  },
  parserOptions: {
    //指定ESLint可以解析JSX语法
    ecmaVersion: 2019,
    sourceType: "module"
  }
};
