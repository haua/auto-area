import axios from 'axios'
import fs from 'fs'
import path from 'path'
import {getAllDatas} from './lib'
const requestPro = require('request-promise-native')

test('获取并生成数据', async () => {
  const areaData = await getAllDatas()

  const date = new Date()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const fileName = `${date.getFullYear()}-${month < 10 ? '0' : ''}${month}-${
    day < 10 ? '0' : ''
  }${day}.json`
  const filePath = path.join(__dirname, `../data`)

  return new Promise(resolve => {
    fs.mkdir(
      path.join(__dirname, `../data`),
      {
        recursive: true,
      },
      () => {
        //
        const fileData = Buffer.from(JSON.stringify(areaData))

        // 块方式写入文件
        const wstream = fs.createWriteStream(path.join(filePath, fileName))

        wstream.on('open', () => {
          const blockSize = 128
          const nbBlocks = Math.ceil(fileData.length / blockSize)
          for (let i = 0; i < nbBlocks; i++) {
            const currentBlock = fileData.slice(
              blockSize * i,
              Math.min(blockSize * (i + 1), fileData.length)
            )
            wstream.write(currentBlock)
          }

          wstream.end()
        })
        wstream.on('error', err => {
          console.error('文件存储失败：', err)
          resolve(false)
        })
        wstream.on('finish', () => {
          console.log('文件存储成功：', path.join(filePath, fileName))
          resolve(true)
        })
      }
    )
  })
})

test('模拟浏览器请求', async () => {
  const url = 'http://localhost:5289/666'
  // const url = 'http://www.mca.gov.cn//article/sj/xzqh/2020/2020/2020092500801.html'
  const res = await axios.get(url, {
    // @ts-ignore
    decompress: false,
    headers: {
      // 指定请求头
      'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36',
      Connection: 'keep-alive',
      Accept:
        'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
      'Accept-Language': 'zh-CN,zh;q=0.8', // 指定 Accept-Language
      Cookie: '', // 指定 Cookie
      referer: '',
    },
  })
  console.log(res)
})

test('模拟浏览器请求2', async () => {
  const url = 'http://localhost:5289/666'
  // const url = 'http://www.mca.gov.cn//article/sj/xzqh/2020/2020/2020092500801.html'
  const res = await requestPro({
    uri: url,
    method: 'GET',
    headers: {
      // 指定请求头
      'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36',
      Connection: 'keep-alive',
      Accept:
        'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
      'Accept-Language': 'zh-CN,zh;q=0.8', // 指定 Accept-Language
      Cookie: '', // 指定 Cookie
    },
  })
  console.log(res)
})

test('检查两个对象是否相等', async () => {
  const d1 = require('../data/2020-11-14.json')
  const d2 = require('../data/2020-11-14.bac.json')
  d1.forEach((d: any, i: number) => {
    if (d.name !== d2[i].name) {
      console.log('名称对应不上', d.name, d2[i].name)
      throw new Error()
    }
    if (d2[i].sid && d.code !== d2[i].sid) {
      console.log('code对应不上', d.code, d2[i].sid)
      throw new Error()
    }
  })
})
