/**
 * 从官方网站同步中国行政区划信息
 * */
// import axios from 'axios'
const requestPro = require('request-promise-native')
const cheerio = require('cheerio')

// 大陆数据由此动态获得（其中金门县和连江县划分在福建省）: http://www.mca.gov.cn/article/sj/xzqh/
const areaDataUrl =
  'http://www.mca.gov.cn//article/sj/xzqh/2020/2020/2020092500801.html'

// 修改港澳台数据时要注意：
// 1. 如果改名，可以直接改
// 2. 如果新增市级，要从最后一个市后面加
// 3. 如果新增县级，要在该市最后一个县后面加
// 4. 如果删除，不要直接删，要在return前删，因为这里会按顺序生成code，如果从中间删了，后面的code全部都对不上以前的code了
/**
 * 香港的区划
 * 根据wiki写死
 * 数据截至： 2018-12-11
 * https://zh.wikipedia.org/wiki/%E9%A6%99%E6%B8%AF%E8%A1%8C%E6%94%BF%E5%8D%80%E5%8A%83#%E5%8D%80%E8%AD%B0%E6%9C%83
 * */
const xiangGangData: SepcialDataT[] = [
  {name: '香港岛', level: 3},
  {name: '中西区', level: 4},
  {name: '湾仔区', level: 4},
  {name: '东区', level: 4},
  {name: '南区', level: 4},

  {name: '九龙', level: 3},
  {name: '油尖旺区', level: 4},
  {name: '深水埗区', level: 4},
  {name: '九龙城区', level: 4},
  {name: '黄大仙区', level: 4},
  {name: '观塘区', level: 4},

  {name: '新界', level: 3},
  {name: '葵青区', level: 4},
  {name: '荃湾区', level: 4},
  {name: '屯门区', level: 4},
  {name: '元朗区', level: 4},
  {name: '北区', level: 4},
  {name: '大埔区', level: 4},
  {name: '沙田区', level: 4},
  {name: '西贡区', level: 4},
  {name: '离岛区', level: 4},
]
/**
 * 加入台湾的市
 * 根据wiki写死
 * 数据截至： 2018-12-11
 * https://zh.wikipedia.org/wiki/%E8%87%BA%E7%81%A3%E8%A1%8C%E6%94%BF%E5%8D%80%E5%8A%83#%E8%87%BA%E7%81%A3%E7%8F%BE%E8%A1%8C%E8%A1%8C%E6%94%BF%E5%8D%80%E5%8A%83
 * */
const taiWanData: SepcialDataT[] = [
  {name: '台北市', level: 3},
  {name: '中正区', level: 4},
  {name: '大同区', level: 4},
  {name: '中山区', level: 4},
  {name: '松山区', level: 4},
  {name: '大安区', level: 4},
  {name: '万华区', level: 4},
  {name: '信义区', level: 4},
  {name: '士林区', level: 4},
  {name: '北投区', level: 4},
  {name: '内湖区', level: 4},
  {name: '南港区', level: 4},
  {name: '文山区', level: 4},

  {name: '新北市', level: 3},
  {name: '板桥区', level: 4},
  {name: '新庄区', level: 4},
  {name: '中和区', level: 4},
  {name: '永和区', level: 4},
  {name: '土城区', level: 4},
  {name: '树林区', level: 4},
  {name: '三峡区', level: 4},
  {name: '莺歌区', level: 4},
  {name: '三重区', level: 4},
  {name: '芦洲区', level: 4},
  {name: '五股区', level: 4},
  {name: '泰山区', level: 4},
  {name: '林口区', level: 4},
  {name: '八里区', level: 4},
  {name: '淡水区', level: 4},
  {name: '三芝区', level: 4},
  {name: '石门区', level: 4},
  {name: '金山区', level: 4},
  {name: '万里区', level: 4},
  {name: '汐止区', level: 4},
  {name: '瑞芳区', level: 4},
  {name: '贡寮区', level: 4},
  {name: '平溪区', level: 4},
  {name: '双溪区', level: 4},
  {name: '新店区', level: 4},
  {name: '深坑区', level: 4},
  {name: '石碇区', level: 4},
  {name: '坪林区', level: 4},
  {name: '乌来区', level: 4},

  {name: '桃园市', level: 3},
  {name: '桃园区', level: 4},
  {name: '中坜区', level: 4},
  {name: '大溪区', level: 4},
  {name: '杨梅区', level: 4},
  {name: '芦竹区', level: 4},
  {name: '大园区', level: 4},
  {name: '龟山区', level: 4},
  {name: '八德区', level: 4},
  {name: '龙潭区', level: 4},
  {name: '平镇区', level: 4},
  {name: '新屋区', level: 4},
  {name: '观音区', level: 4},
  {name: '复兴区', level: 4},

  {name: '台中市', level: 3},
  {name: '中区', level: 4},
  {name: '东区', level: 4},
  {name: '南区', level: 4},
  {name: '西区', level: 4},
  {name: '北区', level: 4},
  {name: '北屯区', level: 4},
  {name: '西屯区', level: 4},
  {name: '南屯区', level: 4},
  {name: '太平区', level: 4},
  {name: '大里区', level: 4},
  {name: '雾峰区', level: 4},
  {name: '乌日区', level: 4},
  {name: '丰原区', level: 4},
  {name: '后里区', level: 4},
  {name: '石冈区', level: 4},
  {name: '东势区', level: 4},
  {name: '和平区', level: 4},
  {name: '新社区', level: 4},
  {name: '潭子区', level: 4},
  {name: '大雅区', level: 4},
  {name: '神冈区', level: 4},
  {name: '大肚区', level: 4},
  {name: '龙井区', level: 4},
  {name: '沙鹿区', level: 4},
  {name: '梧栖区', level: 4},
  {name: '清水区', level: 4},
  {name: '大甲区', level: 4},
  {name: '外埔区', level: 4},
  {name: '大安区', level: 4},

  {name: '台南市', level: 3},
  {name: '东区', level: 4},
  {name: '南区', level: 4},
  {name: '北区', level: 4},
  {name: '安南区', level: 4},
  {name: '安平区', level: 4},
  {name: '中西区', level: 4},
  {name: '新营区', level: 4},
  {name: '盐水区', level: 4},
  {name: '白河区', level: 4},
  {name: '柳营区', level: 4},
  {name: '后壁区', level: 4},
  {name: '东山区', level: 4},
  {name: '麻豆区', level: 4},
  {name: '下营区', level: 4},
  {name: '六甲区', level: 4},
  {name: '官田区', level: 4},
  {name: '大内区', level: 4},
  {name: '佳里区', level: 4},
  {name: '学甲区', level: 4},
  {name: '西港区', level: 4},
  {name: '七股区', level: 4},
  {name: '将军区', level: 4},
  {name: '北门区', level: 4},
  {name: '新化区', level: 4},
  {name: '善化区', level: 4},
  {name: '新市区', level: 4},
  {name: '安定区', level: 4},
  {name: '山上区', level: 4},
  {name: '玉井区', level: 4},
  {name: '楠西区', level: 4},
  {name: '南化区', level: 4},
  {name: '左镇区', level: 4},
  {name: '仁德区', level: 4},
  {name: '归仁区', level: 4},
  {name: '关庙区', level: 4},
  {name: '龙崎区', level: 4},
  {name: '永康区', level: 4},

  {name: '高雄市', level: 3},
  {name: '盐埕区', level: 4},
  {name: '鼓山区', level: 4},
  {name: '左营区', level: 4},
  {name: '楠梓区', level: 4},
  {name: '三民区', level: 4},
  {name: '新兴区', level: 4},
  {name: '前金区', level: 4},
  {name: '苓雅区', level: 4},
  {name: '前镇区', level: 4},
  {name: '旗津区', level: 4},
  {name: '小港区', level: 4},
  {name: '凤山区', level: 4},
  {name: '林园区', level: 4},
  {name: '大寮区', level: 4},
  {name: '大树区', level: 4},
  {name: '大社区', level: 4},
  {name: '仁武区', level: 4},
  {name: '鸟松区', level: 4},
  {name: '冈山区', level: 4},
  {name: '桥头区', level: 4},
  {name: '燕巢区', level: 4},
  {name: '田寮区', level: 4},
  {name: '阿莲区', level: 4},
  {name: '路竹区', level: 4},
  {name: '湖内区', level: 4},
  {name: '茄萣区', level: 4},
  {name: '永安区', level: 4},
  {name: '弥陀区', level: 4},
  {name: '梓官区', level: 4},
  {name: '旗山区', level: 4},
  {name: '美浓区', level: 4},
  {name: '六龟区', level: 4},
  {name: '甲仙区', level: 4},
  {name: '杉林区', level: 4},
  {name: '内门区', level: 4},
  {name: '茂林区', level: 4},
  {name: '桃源区', level: 4},
  {name: '那玛夏区', level: 4},

  {name: '基隆市', level: 3},
  {name: '中正区', level: 4},
  {name: '七堵区', level: 4},
  {name: '暖暖区', level: 4},
  {name: '仁爱区', level: 4},
  {name: '中山区', level: 4},
  {name: '安乐区', level: 4},
  {name: '信义区', level: 4},

  {name: '新竹市', level: 3},
  {name: '东区', level: 4},
  {name: '北区', level: 4},
  {name: '香山区', level: 4},

  {name: '嘉义市', level: 3},
  {name: '东区', level: 4},
  {name: '西区', level: 4},

  {name: '新竹县', level: 3},
  {name: '竹北市', level: 4},
  {name: '关西镇', level: 4},
  {name: '新埔镇', level: 4},
  {name: '竹东镇', level: 4},
  {name: '湖口乡', level: 4},
  {name: '横山乡', level: 4},
  {name: '新丰乡', level: 4},
  {name: '芎林乡', level: 4},
  {name: '宝山乡', level: 4},
  {name: '北埔乡', level: 4},
  {name: '峨眉乡', level: 4},
  {name: '尖石乡', level: 4},
  {name: '五峰乡', level: 4},

  {name: '苗栗县', level: 3},
  {name: '苗栗市', level: 4},
  {name: '头份市', level: 4},
  {name: '苑里镇', level: 4},
  {name: '通霄镇', level: 4},
  {name: '竹南镇', level: 4},
  {name: '后龙镇', level: 4},
  {name: '卓兰镇', level: 4},
  {name: '大湖乡', level: 4},
  {name: '公馆乡', level: 4},
  {name: '铜锣乡', level: 4},
  {name: '南庄乡', level: 4},
  {name: '头屋乡', level: 4},
  {name: '三义乡', level: 4},
  {name: '西湖乡', level: 4},
  {name: '造桥乡', level: 4},
  {name: '三湾乡', level: 4},
  {name: '狮潭乡', level: 4},
  {name: '泰安乡', level: 4},

  {name: '彰化县', level: 3},
  {name: '彰化市', level: 4},
  {name: '员林市', level: 4},
  {name: '和美镇', level: 4},
  {name: '鹿港镇', level: 4},
  {name: '北斗镇', level: 4},
  {name: '溪湖镇', level: 4},
  {name: '田中镇', level: 4},
  {name: '二林镇', level: 4},
  {name: '线西乡', level: 4},
  {name: '伸港乡', level: 4},
  {name: '福兴乡', level: 4},
  {name: '秀水乡', level: 4},
  {name: '花坛乡', level: 4},
  {name: '芬园乡', level: 4},
  {name: '大村乡', level: 4},
  {name: '埔盐乡', level: 4},
  {name: '埔心乡', level: 4},
  {name: '永靖乡', level: 4},
  {name: '社头乡', level: 4},
  {name: '二水乡', level: 4},
  {name: '田尾乡', level: 4},
  {name: '埤头乡', level: 4},
  {name: '芳苑乡', level: 4},
  {name: '大城乡', level: 4},
  {name: '竹塘乡', level: 4},
  {name: '溪州乡', level: 4},

  {name: '南投县', level: 3},
  {name: '南投市', level: 4},
  {name: '埔里镇', level: 4},
  {name: '草屯镇', level: 4},
  {name: '竹山镇', level: 4},
  {name: '集集镇', level: 4},
  {name: '名间乡', level: 4},
  {name: '鹿谷乡', level: 4},
  {name: '中寮乡', level: 4},
  {name: '鱼池乡', level: 4},
  {name: '国姓乡', level: 4},
  {name: '水里乡', level: 4},
  {name: '信义乡', level: 4},
  {name: '仁爱乡', level: 4},

  {name: '云林县', level: 3},
  {name: '斗六市', level: 4},
  {name: '斗南镇', level: 4},
  {name: '虎尾镇', level: 4},
  {name: '西螺镇', level: 4},
  {name: '土库镇', level: 4},
  {name: '北港镇', level: 4},
  {name: '古坑乡', level: 4},
  {name: '大埤乡', level: 4},
  {name: '莿桐乡', level: 4},
  {name: '林内乡', level: 4},
  {name: '二𪨧(仑)乡', level: 4},
  {name: '𪨧(仑)背乡', level: 4},
  {name: '麦寮乡', level: 4},
  {name: '东势乡', level: 4},
  {name: '褒忠乡', level: 4},
  {name: '台西乡', level: 4},
  {name: '元长乡', level: 4},
  {name: '四湖乡', level: 4},
  {name: '口湖乡', level: 4},
  {name: '水林乡', level: 4},

  {name: '嘉义县', level: 3},
  {name: '太保市', level: 4},
  {name: '朴子市', level: 4},
  {name: '布袋镇', level: 4},
  {name: '大林镇', level: 4},
  {name: '民雄乡', level: 4},
  {name: '溪口乡', level: 4},
  {name: '新港乡', level: 4},
  {name: '六脚乡', level: 4},
  {name: '东石乡', level: 4},
  {name: '义竹乡', level: 4},
  {name: '鹿草乡', level: 4},
  {name: '水上乡', level: 4},
  {name: '中埔乡', level: 4},
  {name: '竹崎乡', level: 4},
  {name: '梅山乡', level: 4},
  {name: '番路乡', level: 4},
  {name: '大埔乡', level: 4},
  {name: '阿里山乡', level: 4},

  {name: '屏东县(屏东市)', level: 3},
  {name: '屏东市', level: 4},
  {name: '潮州镇', level: 4},
  {name: '东港镇', level: 4},
  {name: '恒春镇', level: 4},
  {name: '万丹乡', level: 4},
  {name: '长治乡', level: 4},
  {name: '麟洛乡', level: 4},
  {name: '九如乡', level: 4},
  {name: '里港乡', level: 4},
  {name: '盐埔乡', level: 4},
  {name: '高树乡', level: 4},
  {name: '万峦乡', level: 4},
  {name: '内埔乡', level: 4},
  {name: '竹田乡', level: 4},
  {name: '新埤乡', level: 4},
  {name: '枋寮乡', level: 4},
  {name: '新园乡', level: 4},
  {name: '崁顶乡', level: 4},
  {name: '林边乡', level: 4},
  {name: '南州乡', level: 4},
  {name: '佳冬乡', level: 4},
  {name: '琉球乡', level: 4},
  {name: '车城乡', level: 4},
  {name: '满州乡', level: 4},
  {name: '枋山乡', level: 4},
  {name: '三地门乡', level: 4},
  {name: '雾台乡', level: 4},
  {name: '玛家乡', level: 4},
  {name: '泰武乡', level: 4},
  {name: '来义乡', level: 4},
  {name: '春日乡', level: 4},
  {name: '狮子乡', level: 4},
  {name: '牡丹乡', level: 4},

  {name: '宜兰县', level: 3},
  {name: '宜兰市', level: 4},
  {name: '罗东镇', level: 4},
  {name: '苏澳镇', level: 4},
  {name: '头城镇', level: 4},
  {name: '礁溪乡', level: 4},
  {name: '壮围乡', level: 4},
  {name: '员山乡', level: 4},
  {name: '冬山乡', level: 4},
  {name: '五结乡', level: 4},
  {name: '三星乡', level: 4},
  {name: '大同乡', level: 4},
  {name: '南澳乡', level: 4},

  {name: '花莲县', level: 3},
  {name: '花莲市', level: 4},
  {name: '凤林镇', level: 4},
  {name: '玉里镇', level: 4},
  {name: '新城乡', level: 4},
  {name: '吉安乡', level: 4},
  {name: '寿丰乡', level: 4},
  {name: '光复乡', level: 4},
  {name: '丰滨乡', level: 4},
  {name: '瑞穗乡', level: 4},
  {name: '富里乡', level: 4},
  {name: '秀林乡', level: 4},
  {name: '万荣乡', level: 4},
  {name: '卓溪乡', level: 4},

  {name: '台东县', level: 3},
  {name: '台东市', level: 4},
  {name: '成功镇', level: 4},
  {name: '关山镇', level: 4},
  {name: '卑南乡', level: 4},
  {name: '大武乡', level: 4},
  {name: '太麻里乡', level: 4},
  {name: '东河乡', level: 4},
  {name: '长滨乡', level: 4},
  {name: '鹿野乡', level: 4},
  {name: '池上乡', level: 4},
  {name: '绿岛乡', level: 4},
  {name: '延平乡', level: 4},
  {name: '海端乡', level: 4},
  {name: '达仁乡', level: 4},
  {name: '金峰乡', level: 4},
  {name: '兰屿乡', level: 4},

  {name: '澎湖县', level: 3},
  {name: '马公市', level: 4},
  {name: '湖西乡', level: 4},
  {name: '白沙乡', level: 4},
  {name: '西屿乡', level: 4},
  {name: '望安乡', level: 4},
  {name: '七美乡', level: 4},
]
/**
 * 澳门
 * 根据wiki写死
 * 数据截至： 2018-12-11
 * https://zh.wikipedia.org/wiki/%E6%BE%B3%E9%96%80%E8%A1%8C%E6%94%BF%E5%8D%80%E5%8A%83#%E8%A1%8C%E6%94%BF%E5%8D%80%E5%8A%83%E5%88%86
 * */
const aoMenData: SepcialDataT[] = [
  // 澳门特别行政区 澳门半岛 氹仔路 氹城
  {name: '澳门特别行政区', level: 3},
  {name: '澳门半岛', level: 4},
  {name: '氹仔', level: 4},
  {name: '路氹城', level: 4},
  {name: '路环', level: 4},
]

// 没有列入国家行政区划的区，暂时不确定要不要放进去，code也是瞎编的
const sepcialArea = [
  {"level":4,"code":"4403999","pcode":"440300","name":"深汕特别合作区"}
]

/**
 * 分析并生成省市表格
 * http://www.mca.gov.cn/article/sj/xzqh/2018/
 * 只能分析以上地址的数据，以上数据有两种，一种是全部的，一种是变更情况，只能分析全部的
 *
 * http://xzqh.mca.gov.cn/map
 * 看这里能知道具体行政划分
 *
 * 分析来的数据有的市下面如果没有区，则什么都没有
 * 区划代码说明：
 * 一共有6位
 * 前2位是省级代码
 * 中间2位市级代码
 * 后两位是县级代码
 * */
export async function getAllDatas() {
  const data = await getDataLikeChrome(areaDataUrl)
  const $ = cheerio.load(data)
  const lines: string[][] = []

  // console.log($('table tbody'))

  $('table tr').each((i: number, item: any) => {
    const line: string[] = []
    $(item)
      .find('td')
      .each((i: number, item: any) => {
        const text = trim($(item).text().replace())
        if (text) {
          line.push(text)
        }
      })
    if (line[1] && /^[0-9]+$/.test(line[0])) {
      lines.push(line)
    }
  })

  const datas: AreaDataT[] = [
    {level: 0, code: '01', pcode: '', name: '亚洲(Asia)'},
    {level: 0, code: '02', pcode: '', name: '欧洲(Europe)'},
    {level: 0, code: '03', pcode: '', name: '北美洲(North America)'},
    {level: 0, code: '04', pcode: '', name: '南美洲(South America)'},
    {level: 0, code: '05', pcode: '', name: '非洲(Africa)'},
    {level: 0, code: '06', pcode: '', name: '大洋洲(Oceania)'},
    {level: 0, code: '07', pcode: '', name: '南极洲(Antarctica)'},
    {level: 1, code: '0001', pcode: '', name: '中国(China)'},
  ]
  const chinaCode = '0001'
  let nowPath = [chinaCode] // 当前遍历中的区划路径，里面每个项目是每级的code，第一个是国家code
  let nowPathName = ['中国']
  let last: string[] = []

  for (const item of lines) {
    let nowLevel = 2 // 循环中当前循环的区划级别
    const [, lv2, lv3, lv4] = item[0].match(/(..)(..)(..)/) || []
    if (lv2 !== '00' && lv3 === '00' && lv4 === '00') {
      // 这是一个省
      nowLevel = 2
    } else if (lv2 !== '00' && lv3 !== '00' && lv4 === '00') {
      // 这是一个市
      nowLevel = 3
      // if (lv2 !== last[0]) {
      //   // 万一这个市级所属省跟上一个的不一样，则这是一个国家直辖地级市，目前我国没有这种情况，因为国家直辖的市都是省级市。
      //   nowLevel = 2
      // }
    } else {
      // 这是一个县
      if (lv3 !== last[1]) {
        // 万一这个县级所属市跟上一个的不一样，则这是一个省直辖县级
        // 创建一个不存在的地级，否则多级选择时选不中它
        const code = `${lv2}${lv3}00` // 因为这是我创建的虚拟的，所以这个编号也是我编的
        nowLevel = 3
        const addData = {
          level: nowLevel,
          code: code,
          pcode: nowPath[nowLevel - 2] || chinaCode,
          name: nowPathName[nowLevel - 2], // 直接把省的名字拿过来，当成虚拟市的名字
        }
        // 重庆特殊情况：它有两种类型的县级，一种叫县，一种叫区，id中间两位不一样。
        if (lv2 === '50') {
          if (lv3 === '01') {
            addData.name = '区'
          } else {
            addData.name = '县'
          }
        }
        datas.push(addData)
        nowPath[nowLevel - 1] = code
        nowPathName[nowLevel - 1] = addData.name
      }
      nowLevel = 4
    }

    last = [lv2, lv3, lv4]

    // 设置当前区划，方便后续循环使用
    nowPath[nowLevel - 1] = item[0]
    nowPathName[nowLevel - 1] = item[1]

    datas.push({
      level: nowLevel,
      code: item[0],
      pcode: nowPath[nowLevel - 2],
      name: item[1],
    })

    if (
      // ['东莞市', '中山市', '三沙市', '儋州市', '嘉峪关市'].includes(item[1])
      ['441900', '442000', '460300', '460400', '620200'].includes(item[0])
    ) {
      const moreDatas = getOtherDatas(item[1], [lv2, lv3])
      datas.push(...moreDatas)
    }
  }

  // 台湾
  const taiWanDatas = await getSpecialDatas('71', taiWanData)
  if (taiWanDatas.length) {
    datas.push(...taiWanDatas)
  }

  // 香港
  const xiangGangDatas = await getSpecialDatas('81', xiangGangData)
  if (xiangGangDatas.length) {
    datas.push(...xiangGangDatas)
  }

  // 澳门
  const aoMenDatas = await getSpecialDatas('82', aoMenData)
  if (aoMenDatas.length) {
    datas.push(...aoMenDatas)
  }

  return datas
}

/**
 * 加入台湾的市
 * 根据wiki写死
 * https://zh.wikipedia.org/wiki/%E8%87%BA%E7%81%A3%E8%A1%8C%E6%94%BF%E5%8D%80%E5%8A%83#%E8%87%BA%E7%81%A3%E7%8F%BE%E8%A1%8C%E8%A1%8C%E6%94%BF%E5%8D%80%E5%8A%83
 *
 * 数据截至： 2018-12-11
 * 里面的code是我定义的，以后如果官方出code了，会改成官方的code
 * @param areaID 这类数据的父级id
 * @param ds 这类数据的父级id
 * */
export async function getSpecialDatas(areaID: string, ds: SepcialDataT[]) {
  const datas = []
  let id = 0
  let lastCityId = 0
  let pcode = ''
  for (const item of ds) {
    let code = areaID
    if (item.level === 3) {
      lastCityId++
      code += (lastCityId < 10 ? '0' : '') + lastCityId.toString() + '00'
      pcode = code
      id = 0
    } else {
      id++
      code += (id < 10 ? '0' : '') + id.toString()
    }
    datas.push({
      level: item.level,
      code: code,
      pcode: item.level === 3 ? areaID + '0000' : pcode,
      name: item.name,
    })
  }
  return datas
}

/**
 * 获取不设区的地级市的下一级数据
 * 一共是：东莞市、中山市、三沙市、儋州市、嘉峪关市
 * @param city 城市名，拼音小写
 * @param codes 城市及它前面级别的code
 * */
export function getOtherDatas(city: string, codes: string[]) {
  const allDatas = {
    东莞市: [
      '莞城街道',
      '东城街道',
      '南城街道',
      '万江街道',
      '石碣镇',
      '高埗镇',
      '麻涌镇',
      '洪梅镇',
      '中堂镇',
      '望牛墩镇',
      '道滘镇',
      '虎门镇',
      '长安镇',
      '厚街镇',
      '沙田镇',
      '东莞港开发区',
      '石龙镇',
      '石排镇',
      '茶山镇',
      '寮步镇',
      '大岭山镇',
      '大朗镇',
      '松山湖高新区',
      '常平镇',
      '黄江镇',
      '企石镇',
      '桥头镇',
      '横沥镇',
      '谢岗镇',
      '东部工业园',
      '塘厦镇',
      '清溪镇',
      '樟木头镇',
      '凤岗镇',
    ],
    中山市: [
      '石岐街道',
      '东区街道',
      '西区街道',
      '南区街道',
      '五桂山街道',
      '中山火炬高技术产业开发区',
      '黄圃镇',
      '南头镇',
      '东凤镇',
      '阜沙镇',
      '小榄镇',
      '东升镇',
      '古镇镇',
      '横栏镇',
      '三角镇',
      '民众镇',
      '南朗镇',
      '港口镇',
      '大涌镇',
      '沙溪镇',
      '三乡镇',
      '板芙镇',
      '神湾镇',
      '坦洲镇',
    ],
    三沙市: ['西沙群岛', '中沙群岛', '南沙群岛'],
    儋州市: [
      '那大镇',
      '和庆镇',
      '南丰镇',
      '大成镇',
      '雅星镇',
      '兰洋镇',
      '光村镇',
      '木棠镇',
      '海头镇',
      '峨蔓镇',
      '王五镇',
      '白马井镇',
      '中和镇',
      '排浦镇',
      '东成镇',
      '新州镇',
      '国营八一总场',
      '国营蓝洋农场',
      '国营西联农场',
      '国营西培农场',
    ],
    嘉峪关市: [
      '立雄关区',
      '长城区',
      '镜铁区',
      '胜利街道',
      '五一街道',
      '矿山街道',
      '新华街道',
      '建设街道',
      '前进街道',
      '峪苑街道',
      '朝阳街道',
      '峪泉镇',
      '文殊镇',
      '新城镇',
    ],
  }

  const ds = allDatas[city as keyof typeof allDatas]
  if (!ds) {
    return []
  }

  let id = 0
  const pcode = codes.join('') + '00'
  const datas = []
  for (const item of ds) {
    id++
    datas.push({
      level: 4,
      code: codes.join('') + (id < 10 ? '0' : '') + id.toString(),
      pcode,
      name: item,
    })
  }
  return datas
}

/**
 * 模拟浏览器把数据加载回来，加载回的数据不会处理。
 * */
export async function getDataLikeChrome(
  url: string,
  ua?: string,
  qs?: any,
  cookies?: any
) {
  if (cookies) {
    const cookiesArr = []
    for (const [k, v] of Object.entries(cookies)) {
      cookiesArr.push(`${k}=${v}`)
    }
    cookies = cookiesArr.join('; ')
  }
  // return axios.get(url, {
  //   params: qs || {},
  //   // @ts-ignore
  //   decompress: false,
  //   headers: {
  //     // 指定请求头
  //     'User-Agent':
  //       ua ||
  //       'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36',
  //     Connection: 'keep-alive',
  //     Accept:
  //       'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  //     'Accept-Language': 'zh-CN,zh;q=0.8', // 指定 Accept-Language
  //     Cookie: cookies || ' 1;', // 指定 Cookie
  //     referer: '',
  //   },
  // })

  return await requestPro({
    uri: url,
    method: 'GET',
    headers: {
      // 指定请求头
      'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36',
      Connection: 'keep-alive',
      Accept:
        'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
      'Accept-Language': 'zh-CN,zh;q=0.8', // 指定 Accept-Language
      Cookie: '', // 指定 Cookie
    },
  })
}

function trim(str: string) {
  return str.replace(/^\s+|\s+$/g, '')
}

export interface AreaDataT {
  level: number
  code: string
  pcode: string
  name: string
}

interface SepcialDataT {
  name: string
  level: 3 | 4
}
