module.exports = {
  "printWidth": 80,
  "semi": false,
  "singleQuote": true,
  "trailingComma": "es5", // 最后一行是否强制加逗号
  "bracketSpacing": false,
  "jsxBracketSameLine": true,
  "arrowParens": "avoid",
  "tabWidth": 2,
  "useTabs": false
}
